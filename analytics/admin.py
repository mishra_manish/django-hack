from django.contrib import admin
from .models import UserTrack,UrlVisited
# Register your models here.
admin.site.register(UserTrack)
admin.site.register(UrlVisited)