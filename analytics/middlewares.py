"""
Time/View Count Analytics Middleware

This module provides a middleware that implements request-response time and view count
"""
import time

from django.urls import resolve
from django.utils.deprecation import MiddlewareMixin
from .models import UserTrack, UrlVisited


class TimingMiddleware(MiddlewareMixin):
    """
    calculating time difference
    """
    response_t = time.time()

    def process_response(self, request, response):
        current_time = time.time()
        response_time = current_time - request.start_time
        self.response_t = response_time
        return response

    def process_request(self, request):
        request.start_time = time.time()
        view_name = resolve(request.path).func.__name__
        obj = UserTrack.objects.create(session_start_time=request.start_time, page_loading_time=self.response_t,)
        obj.save()

        obj_track = UrlVisited.objects.create(user=obj, view_name=view_name)
        obj_track.save()

