from django.db import models


# Create your models here.


class UserTrack(models.Model):
    user_id = models.TextField(auto_created=True)  # it is for unique user id
    session_start_time = models.DateTimeField(auto_now=True)
    session_end_time = models.DateTimeField(blank=True)
    page_loading_time = models.IntegerField(blank=True)
    no_of_page_visit = models.IntegerField(blank=True)
    ip = models.CharField(max_length=1000,blank=True)

    def __str__(self):
        return str(self.user_id) #bhag gaya kya??
# ModuleNotFoundError: No module named 'django_analytics.wsgi' is error ne jan kha rakhi he spellling dekh theek he
#hmm ruk force e kya hota? abe matlab ye conflict nahi hone deta push kar deta heouuheroku logs --tail

class UrlVisited(models.Model):  # it store the url visited how many times with given time
    user = models.ForeignKey(UserTrack, on_delete=models.CASCADE)
    url = models.URLField(blank=True, null=True)
    view_name = models.CharField(max_length=100, blank=True)
    count = models.IntegerField(blank=True)
    date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.url)
