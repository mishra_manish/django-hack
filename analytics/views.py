import time

from django.http import HttpResponse

# Create your views here.
from view_analytics.decorators import capture


@capture
def index1(request):
    time.sleep(10)
    return HttpResponse("hello")