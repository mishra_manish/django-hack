from django.contrib import admin
from .models import QView, ViewLog, Count, OnlineErrorLog

# Register your models here.
admin.site.register(QView)
admin.site.register(ViewLog)
admin.site.register(Count)
admin.site.register(OnlineErrorLog)
