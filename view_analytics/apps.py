from django.apps import AppConfig


class ViewAnalyticsConfig(AppConfig):
    name = 'view_analytics'
