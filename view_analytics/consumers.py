import json

from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
from channels.layers import get_channel_layer


# Consumers(maybe admin) that are seeing no of users
class ChatConsumer(WebsocketConsumer):
    def connect(self, **kwargs):
        self.accept()
        async_to_sync(self.channel_layer.group_add)("admin", self.channel_name)

    def disconnect(self, close_code):
        async_to_sync(self.channel_layer.group_discard)("admin", self.channel_name)

    def analytics_admin_message(self, something):
        if something["message"] == "plus":
            self.send(text_data=json.dumps({
                'message': "plus"
            }))

        else:
            self.send(text_data=json.dumps({
                'message': "minus"
            }))


# Those users that are accessing the page page_check
class PageConsumer(WebsocketConsumer):
    def connect(self, **kwargs):
        self.accept()
        channel_layer = get_channel_layer()
        async_to_sync(channel_layer.group_send)("admin", {"type": "analytics.admin_message", "message": "plus"})

    def disconnect(self, close_code):
        channel_layer = get_channel_layer()
        async_to_sync(channel_layer.group_send)("admin", {"type": "analytics.admin_message", "message": "minus"})


class ErrorLogConsumer(WebsocketConsumer):
    def connect(self, **kwargs):
        self.accept()
        async_to_sync(self.channel_layer.group_add)("error_admin", self.channel_name)

    def disconnect(self, close_code):
        async_to_sync(self.channel_layer.group_discard)("error_admin", self.channel_name)

    def analytics_error_log(self, something):
        object_received = something["object"]
        self.send(text_data=json.dumps(
            {
                "error_object": object_received
            }))
