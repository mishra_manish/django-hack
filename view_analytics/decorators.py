import os
import sys
from datetime import datetime
from functools import wraps

from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.forms import model_to_dict
from django.http import HttpResponse
from django.urls import resolve

from view_analytics.models import QView, ViewLog, OnlineErrorLog


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def capture(f):
    @wraps(f)
    def wrapped(*args, **kwargs):
        request = args[0]
        current_url = resolve(request.path_info).url_name
        view_name = resolve(request.path).func.__name__
        obj, created = QView.objects.get_or_create(name=view_name, url_name=current_url)
        start_time = datetime.now()
        r = f(*args, **kwargs)
        vl = ViewLog.objects.create(view=obj,
                                    url_hit=request.build_absolute_uri(),
                                    request_time=start_time,
                                    response_time=datetime.now(),
                                    duration=datetime.now() - start_time,
                                    ip=get_client_ip(request)
                                    )
        return r

    return wrapped


def capture_errors(f):
    @wraps(f)
    def wrapped(*args, **kwargs):
        # request = args[0]
        try:
            r = f(*args, **kwargs)
            return r
        except:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return HttpResponse("Exception Received")

    return wrapped


def show_errors(force_flag=False, own_error_name=None):
    exc_type, exc_obj, exc_tb = sys.exc_info()
    try:
        fname = exc_tb.tb_frame.f_code.co_filename
        error_name = exc_obj.__class__.__name__
        file_name = fname
        line_no = exc_tb.tb_lineno
        if own_error_name is not None:
            error_name = own_error_name

        oel = OnlineErrorLog.objects.create(name=error_name, file_location=file_name, line_no=str(line_no))
        channel_layer = get_channel_layer()
        data = model_to_dict(oel)
        data["timestamp"] = str(str(oel.timestamp.date()) + "," + str(oel.timestamp.time()))
        async_to_sync(channel_layer.group_send)("error_admin", {"type": "analytics.error_log",
                                                                "object": data})

    except:
        if force_flag:
            error_name = "None"
            file_name = "None"
            line_no = "None"

            if own_error_name is not None:
                error_name = own_error_name

            oel = OnlineErrorLog.objects.create(name=error_name, file_location=file_name, line_no=str(line_no))
            channel_layer = get_channel_layer()
            data = model_to_dict(oel)
            data["timestamp"] = str(str(oel.timestamp.date()) + "," + str(oel.timestamp.time()))
            async_to_sync(channel_layer.group_send)("error_admin", {"type": "analytics.error_log",
                                                                    "object": data})
