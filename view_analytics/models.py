from django.db import models


# Create your models here.
class QView(models.Model):
    name = models.CharField(max_length=100)
    url_name = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        unique_together = ('name', 'url_name')


class ViewLog(models.Model):
    view = models.ForeignKey(QView, on_delete=models.CASCADE)
    url_hit = models.URLField()
    request_time = models.DateTimeField()
    response_time = models.DateTimeField()
    duration = models.DurationField()
    ip = models.CharField(max_length=100, default="0.0.0.0")

    def __str__(self):
        return str(self.view) + " " + str(self.duration)


class Count(models.Model):
    user_count = models.IntegerField()


class OnlineErrorLog(models.Model):
    name = models.CharField(max_length=200, null=True, blank=True)
    file_location = models.CharField(max_length=200, null=True, blank=True)
    line_no = models.CharField(max_length=10, null=True, blank=True)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.name) + "," + str(self.file_location) + ", line=" + str(self.line_no) + \
               ", " + str(self.timestamp.date()) + "," + str(self.timestamp.time())
