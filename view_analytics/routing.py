from django.conf.urls import url
from . import consumers

websocket_urlpatterns = [
    url(r'^ws/chat/room/$', consumers.ChatConsumer),
    url(r'^ws/chat/page/$', consumers.PageConsumer),
    url(r'^ws/chat/errors/$', consumers.ErrorLogConsumer),
]