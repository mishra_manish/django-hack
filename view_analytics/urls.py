from django.urls import re_path

from . import views

urlpatterns = [
    re_path('^q', views.view_details),
    re_path('home/$', views.home_page, name="home"),
    re_path('page_check/$', views.page_under_influence, name="realtimecheck"),
    re_path('realtime/$', views.real_time_analytics, name="realtime"),
    re_path('plus/$', views.plus, name='plus'),
    re_path('minus/$', views.minus, name='minus'),
    re_path('count/$', views.count, name='count'),
    re_path("testing/$", views.testing_function, name="testing"),
    re_path("realtime_errors/$", views.realtime_error_analytics, name="realtime_errors"),
]
