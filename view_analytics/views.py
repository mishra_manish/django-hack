from django.db.models import Avg
# from channels import Group
# Create your views here.
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from view_analytics.decorators import capture_errors, show_errors
from view_analytics.models import QView, ViewLog, OnlineErrorLog
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.db.models import Avg
from django.http import HttpResponse
from django.shortcuts import render
from view_analytics.models import QView, ViewLog, Count
import asyncio


# ruk


def view_details(request):
    view = QView.objects.first()
    view_count = ViewLog.objects.filter(view=view).count()
    response_time = ViewLog.objects.filter(view=view).aggregate(Avg("duration"))
    hourly_count = {}
    monthly_count = {}
    date_count = {}
    ip_addresses = {}
    for i in ViewLog.objects.filter(view=view).all():
        try:
            hourly_count[i.request_time.time().hour] += 1
        except KeyError:
            hourly_count[i.request_time.time().hour] = 1

        try:
            monthly_count[i.request_time.month] += 1
        except KeyError:
            monthly_count[i.request_time.month] = 1

        try:
            date_count[i.request_time.day] += 1
        except KeyError:
            date_count[i.request_time.day] = 1

        try:
            ip_addresses[i.ip] += 1
        except:
            ip_addresses[i.ip] = 1

    data = {
        'count': view_count,
        "response_time": response_time,
        "hourly_count": hourly_count,
        "daily_count": date_count,
        "monthly_count": monthly_count,
        "distinct_ip": len(ip_addresses)
    }

    return render(request, "view_analytics/index.html", context=data)


# Twisted ==18.7.0
# httplib == 0.11.3

def home_page(request):
    channel_layer = get_channel_layer()
    # async_to_sync(channel_layer.group_send)("admin", {"type": "analytics.admin_message", "message": "plus"})
    return render(request, 'home/home.html')


def page_under_influence(request):
    return render(request, "view_analytics/page_under_influence.html")


def real_time_analytics(request):
    return render(request, "view_analytics/realtime.html")


def realtime_error_analytics(request):
    errors = OnlineErrorLog.objects.all()
    context_dictionary = {
        "errors": errors
    }
    return render(request, "view_analytics/error_realtime.html", context=context_dictionary)


@csrf_exempt
def plus(request):
    if request.method == "GET":
        obj = Count.objects.first()
        obj.user_count = obj.user_count + 1
        obj.save()
        return HttpResponse(obj.user_count)
    return HttpResponse("hello")


@csrf_exempt
def minus(request):
    if request.method == "GET":
        obj = Count.objects.first()
        obj.user_count = obj.user_count - 1
        obj.save()
        return HttpResponse(obj.user_count)

    return HttpResponse("hello")


@csrf_exempt
def count(request):
    if request.method == "GET":
        obj = Count.objects.first()

        return HttpResponse(obj.user_count)

    return HttpResponse("hello")


@capture_errors
def testing_function(request):
    try:
        print(4 / 0)
    except:
        show_errors(own_error_name="zeroerr")

    return HttpResponse("end")
